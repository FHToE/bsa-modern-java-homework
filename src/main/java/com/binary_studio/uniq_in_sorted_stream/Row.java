package com.binary_studio.uniq_in_sorted_stream;

//You CAN modify this class
public final class Row<RowData> {

	private final Long id;

	public Row(Long id) {
		this.id = id;
	}

	public Long getPrimaryId() {
		return this.id;
	}
	/*
	 * @Override public boolean equals(Object o) { if (o == null || !(o instanceof Row)) {
	 * return false; } return this.id.equals(((Row) o).id); }
	 *
	 * @Override public String toString() { return "" + this.id; }
	 *
	 * @Override public int hashCode() { return this.id.intValue(); }
	 */

}
