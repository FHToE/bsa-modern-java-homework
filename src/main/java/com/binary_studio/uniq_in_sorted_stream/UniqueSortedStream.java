package com.binary_studio.uniq_in_sorted_stream;

import java.util.*;
import java.util.function.*;
import java.util.stream.Stream;
//import java.util.stream.Collectors;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	static Long checkBox;

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		/*
		 * variant with forEach() method List<Row<T>> list = new ArrayList<>();
		 * stream.distinct().forEach(list::add);
		 */

		/*
		 * variant with collect() method List<Row<T>> list =
		 * stream.distinct().collect(Collectors.toList());
		 */

		/*
		 * variant without collect() method List<Row<T>> list = new ArrayList<>();
		 * Iterator<Row<T>> it = stream.distinct().iterator(); while (it.hasNext()) {
		 * list.add(it.next()); }
		 */

		// variant without distinct() method
		/*
		 * List<Row<T>> list = new ArrayList<>(); List<Long> checkList = new
		 * ArrayList<>(); Iterator<Row<T>> it = stream.iterator(); while (it.hasNext()) {
		 * Row<T> element = it.next(); if (!checkList.contains(element.getPrimaryId())) {
		 * checkList.add(element.getPrimaryId()); list.add(element); } } return
		 * list.stream();
		 */

		// general option
		// var uniqSet = new HashSet<Long>();
		// Predicate<Row<T>> checker = x -> {
		// if (uniqSet.contains(x.getPrimaryId())) {
		// return false;
		// }
		// else {
		// uniqSet.add(x.getPrimaryId());
		// return true;
		// }
		// };

		// O(1) or memory option
		Predicate<Row<T>> checker = x -> {
			if (x.getPrimaryId().equals(checkBox)) {
				return false;
			}
			else {
				checkBox = x.getPrimaryId();
				return true;
			}
		};

		checkBox = null;

		return stream.filter(checker);
	}

}
