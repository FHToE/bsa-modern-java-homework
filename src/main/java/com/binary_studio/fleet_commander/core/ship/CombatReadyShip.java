package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private int startShieldHP;

	private int startHullHP;

	private PositiveInteger currentShieldHP;

	private PositiveInteger currentHullHP;

	private int startCapacitorAmount;

	private PositiveInteger currentCapacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger powergridOutput;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSystem;

	private DefenciveSubsystem defSystem;

	protected CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger powergridOutput,
			PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSystem, DefenciveSubsystem defSystem) {
		this.name = name;
		this.startShieldHP = shieldHP.value();
		this.startHullHP = hullHP.value();
		this.currentShieldHP = shieldHP;
		this.currentHullHP = hullHP;
		this.startCapacitorAmount = capacitorAmount.value();
		this.currentCapacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.powergridOutput = powergridOutput;
		this.speed = speed;
		this.size = size;
		this.attackSystem = attackSystem;
		this.defSystem = defSystem;
	}

	@Override
	public void endTurn() {
		this.currentCapacitorAmount = this.startCapacitorAmount
				- this.currentCapacitorAmount.value() > this.capacitorRechargeRate.value()
						? PositiveInteger.of(this.currentCapacitorAmount.value() + this.capacitorRechargeRate.value())
						: PositiveInteger.of(this.startCapacitorAmount);
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		Optional<AttackAction> myAttack = null;
		if (this.currentCapacitorAmount.value() < this.attackSystem.getCapacitorConsumption().value()) {
			myAttack = Optional.empty();
			return myAttack;
		}

		myAttack = Optional.of(new AttackAction(this.attackSystem.attack(target), this, target, this.attackSystem));
		this.currentCapacitorAmount = PositiveInteger
				.of(this.currentCapacitorAmount.value() - this.attackSystem.getCapacitorConsumption().value()); // mby
																												// dont
																												// need
																												// this
																												// action?
		return myAttack;
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		PositiveInteger recievedDamage = this.defSystem.reduceDamage(attack).damage;
		AttackResult result = null;
		if (this.currentShieldHP.value() >= recievedDamage.value()) {
			this.currentShieldHP = PositiveInteger.of(this.currentShieldHP.value() - recievedDamage.value());
		}
		else if (this.currentHullHP.value() <= recievedDamage.value() - this.currentShieldHP.value()) {
			this.currentShieldHP = PositiveInteger.of(0);
			this.currentHullHP = PositiveInteger.of(0);
			result = new AttackResult.Destroyed();
			return result;
		}
		else {
			this.currentHullHP = PositiveInteger
					.of(this.currentHullHP.value() - recievedDamage.value() + this.currentShieldHP.value());
			this.currentShieldHP = PositiveInteger.of(0);
		}
		result = new AttackResult.DamageRecived(attack.weapon, recievedDamage, attack.target);
		return result;
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		Optional<RegenerateAction> optRegAction = null;
		if (this.currentCapacitorAmount.value() < this.defSystem.getCapacitorConsumption().value()) {
			optRegAction = Optional.empty();
			return optRegAction;
		}
		this.currentCapacitorAmount = PositiveInteger
				.of(this.currentCapacitorAmount.value() - this.defSystem.getCapacitorConsumption().value());
		RegenerateAction regAction = this.defSystem.regenerate();

		int intShieldHpAfterRegeneration;
		int intHullHpAfterRegeneration;
		int intShieldHpRegenerated;
		int intHullHpRegenerated;

		if (this.currentShieldHP.value() + regAction.shieldHPRegenerated.value() > this.startShieldHP) {
			intShieldHpAfterRegeneration = this.startShieldHP;
			intShieldHpRegenerated = this.startShieldHP - this.currentShieldHP.value();
		}
		else {
			intShieldHpAfterRegeneration = this.currentShieldHP.value() + regAction.shieldHPRegenerated.value();
			intShieldHpRegenerated = regAction.shieldHPRegenerated.value();
		}

		if (this.currentHullHP.value() + regAction.hullHPRegenerated.value() > this.startHullHP) {
			intHullHpAfterRegeneration = this.startHullHP;
			intHullHpRegenerated = this.startHullHP - this.currentHullHP.value();
		}
		else {
			intHullHpAfterRegeneration = this.currentHullHP.value() + regAction.hullHPRegenerated.value();
			intHullHpRegenerated = regAction.hullHPRegenerated.value();
		}
		this.currentShieldHP = PositiveInteger.of(intShieldHpAfterRegeneration);
		this.currentHullHP = PositiveInteger.of(intHullHpAfterRegeneration);

		optRegAction = Optional.of(new RegenerateAction(PositiveInteger.of(intShieldHpRegenerated),
				PositiveInteger.of(intHullHpRegenerated)));
		return optRegAction;
	}

}
