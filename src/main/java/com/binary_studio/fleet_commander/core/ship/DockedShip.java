package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger powergridOutput;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSystem;

	private DefenciveSubsystem defSystem;

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.powergridOutput = powergridOutput;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		this.attackSystem = subsystem;
		Integer currentPG = (subsystem != null ? subsystem.getPowerGridConsumption().value() : 0)
				+ (this.defSystem != null ? this.defSystem.getPowerGridConsumption().value() : 0);
		if (currentPG > this.powergridOutput.value()) {
			throw new InsufficientPowergridException(currentPG - this.powergridOutput.value());
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		this.defSystem = subsystem;
		Integer currentPG = (subsystem != null ? subsystem.getPowerGridConsumption().value() : 0)
				+ (this.attackSystem != null ? this.attackSystem.getPowerGridConsumption().value() : 0);
		if (currentPG > this.powergridOutput.value()) {
			throw new InsufficientPowergridException(currentPG - this.powergridOutput.value());
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSystem == null && this.defSystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (this.attackSystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		if (this.defSystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}

		CombatReadyShip newInstance = new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.capacitorAmount,
				this.capacitorRechargeRate, this.powergridOutput, this.speed, this.size, this.attackSystem,
				this.defSystem);
		return newInstance;
	}

}
