package com.binary_studio.tree_max_depth;

import java.util.*;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	private static boolean hasSub(Department nodeDepartment) {
		boolean has = false;
		if (nodeDepartment.subDepartments == null || nodeDepartment.subDepartments.size() == 0) {
			return has;
		}
		for (Department childDeparment : nodeDepartment.subDepartments) {
			if (childDeparment != null) {
				has = true;
				return has;
			}
		}
		return has;
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}

		if (rootDepartment.subDepartments.size() == 0) {
			return 1;
		}

		/*
		 * recursive variant
		 *
		 * List<Integer> departmentDepth = new ArrayList<>(); for (Department
		 * childDeparment : rootDepartment.subDepartments) { Integer count =
		 * calculateMaxDepth(childDeparment); departmentDepth.add(count); } return
		 * Collections.max(departmentDepth) + 1;
		 */

		// iterative variant
		Map<Department, Department> depMap = new HashMap<>();
		var depQueue = new ArrayDeque<Department>();
		depQueue.offer(rootDepartment);
		Department lowestDepartment = null;
		while (!depQueue.isEmpty()) {
			Department nodeDepartment = depQueue.poll();
			if (hasSub(nodeDepartment)) {
				for (Department childDeparment : nodeDepartment.subDepartments) {
					if (childDeparment != null) {
						depQueue.offer(childDeparment);
						depMap.put(childDeparment, nodeDepartment);
						lowestDepartment = childDeparment;
					}
				}
			}
		}

		int counter = 1;
		while (depMap.containsKey(lowestDepartment)) {
			lowestDepartment = depMap.get(lowestDepartment);
			counter++;
		}

		return counter;
	}

}
